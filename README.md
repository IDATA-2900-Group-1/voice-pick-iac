# voice-pick-iac

This repository contains the infrastructure as code for Group1's project using Terraform. The IAC code is written in HCL (HashiCorp Configuration Language) and is used to provision and manage the infrastructure resources on the cloud platform.

## Getting Started

### Prerequisites

To run the code in this repository, you must have the following tools installed:

- Terraform (>= v1.1.0)
- A cloud provider account Azure.
- Access credentials for the cloud provider
- az cli installed and logged in

### Installation
```terraform
    terraform init
```

```terraform
    terraform plan
```

```terraform
    terraform apply
```
