# values found in gitlab variables

variable "resource_group_name" {
  default = "${resource_group_name}"
}

variable "admin_name" {
  default = "${admin_name}"
}

variable "subscription_id" {
  default = "${subscription_id}"
}